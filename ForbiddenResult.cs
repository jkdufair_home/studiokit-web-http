﻿using StudioKit.ErrorHandling.WebApi;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Web.Http
{
	/// <summary>
	///     Represents an IHttpActionResult that returns a <see cref="HttpStatusCode.Forbidden" /> response.
	///     Returns an <see cref="ApiErrorMessage" />
	/// </summary>
	public class ForbiddenResult : IHttpActionResult
	{
		private readonly HttpRequestMessage _request;
		private readonly string _reasonPhrase;

		/// <summary>
		///     Initializes a new instance of the <see cref="ForbiddenResult" /> class.
		/// </summary>
		/// <param name="request">The request message which led to this result.</param>
		/// <param name="reasonPhrase">The reason phrase for the response.</param>
		public ForbiddenResult(HttpRequestMessage request, string reasonPhrase = null)
		{
			_request = request;
			_reasonPhrase = reasonPhrase;
		}

		#region IHttpActionResult Members

		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			return
				Task.FromResult(
					_request.CreateResponse(new ApiErrorMessage(HttpStatusCode.Forbidden,
						"Access Denied",
						$"Your request has been denied.{(_reasonPhrase != null ? " " : "")}{_reasonPhrase}")));
		}

		#endregion IHttpActionResult Members
	}
}