﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace StudioKit.Web.Http
{
	/// <summary>
	///     Represents an IHttpActionResult that returns a File.
	/// </summary>
	public class FileResult : IHttpActionResult
	{
		private readonly HttpRequestMessage _request;
		private readonly string _fileName;
		private readonly HttpContent _content;

		/// <summary>
		///     Initializes a new instance of the <see cref="FileResult" /> class.
		/// </summary>
		/// <param name="request">The request message which led to this result.</param>
		/// <param name="fileName">The fileName to be sent in the Response Headers.</param>
		/// <param name="content">The CSV content as a string.</param>
		public FileResult(HttpRequestMessage request, string fileName, HttpContent content)
		{
			_request = request;
			_fileName = fileName;
			_content = content;
		}

		#region IHttpActionResult Members

		public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
		{
			var responseMessage = _request.CreateResponse(HttpStatusCode.OK);
			responseMessage.Content = _content;
			responseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
			responseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
			{
				FileName = _fileName
			};
			return Task.FromResult(responseMessage);
		}

		#endregion IHttpActionResult Members
	}
}