﻿using Ionic.Zlib;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace StudioKit.Web.Http.Filters
{
	public class GzipFilterAttribute : ActionFilterAttribute
	{
		public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext,
			CancellationToken cancellationToken)
		{
			if (actionExecutedContext.Response == null ||
				actionExecutedContext.Response.Content?.Headers?.ContentDisposition?.DispositionType == "attachment")
			{
				await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
				return;
			}
			var content = actionExecutedContext.Response.Content;
			var bytes = content == null ? null : await content.ReadAsByteArrayAsync();
			var gzippedContent = bytes == null ? new byte[0] : CompressionHelper.Gzip(bytes);
			actionExecutedContext.Response.Content = new ByteArrayContent(gzippedContent);
			if (content != null)
			{
				foreach (var httpContentHeader in content.Headers)
				{
					actionExecutedContext.Response.Content.Headers.Add(httpContentHeader.Key, httpContentHeader.Value);
				}
			}
			actionExecutedContext.Response.Content.Headers.Add("Content-encoding", "gzip");
			await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
		}
	}

	public class CompressionHelper
	{
		public static byte[] Gzip(byte[] bytes)
		{
			if (bytes == null)
			{
				return null;
			}

			using (var output = new MemoryStream())
			{
				using (var compressor = new GZipStream(output, CompressionMode.Compress,
					CompressionLevel.BestSpeed))
				{
					compressor.Write(bytes, 0, bytes.Length);
				}
				return output.ToArray();
			}
		}
	}
}