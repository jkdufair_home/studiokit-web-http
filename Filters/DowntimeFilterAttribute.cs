﻿using StudioKit.ErrorHandling.WebApi;
using System.Net;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace StudioKit.Web.Http.Filters
{
	public class DowntimeFilterAttribute : AuthorizationFilterAttribute
	{
		public override void OnAuthorization(HttpActionContext actionContext)
		{
			actionContext.Response =
				actionContext.Request.CreateResponse(new ApiErrorMessage(HttpStatusCode.Forbidden,
					"We'll be right back!",
					"Our developers are currently hard at work. Normal service will be restored soon."));
		}
	}
}