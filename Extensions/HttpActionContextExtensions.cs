﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http.Controllers;

namespace StudioKit.Web.Http.Extensions
{
	public static class HttpActionContextExtensions
	{
		public static bool IsPrincipalAuthenticated(this HttpActionContext actionContext)
		{
			var identity = (ClaimsIdentity)actionContext.RequestContext.Principal.Identity;
			return identity.IsAuthenticated;
		}

		public static bool IsPrincipalAuthenticatedUser(this HttpActionContext actionContext)
		{
			var identity = (ClaimsIdentity)actionContext.RequestContext.Principal.Identity;
			return identity.IsAuthenticated && identity.Claims.Any(c => c.Type == ClaimTypes.Email.ToString());
		}
	}
}