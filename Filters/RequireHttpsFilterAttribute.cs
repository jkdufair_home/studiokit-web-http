﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace StudioKit.Web.Http.Filters
{
	public class RequireHttpsFilterAttribute : AuthorizationFilterAttribute
	{
		public override void OnAuthorization(HttpActionContext actionContext)
		{
			if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
			{
				actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden)
				{
					ReasonPhrase = "HTTPS Required"
				};
			}
			else
			{
				base.OnAuthorization(actionContext);
			}
		}
	}
}