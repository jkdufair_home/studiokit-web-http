﻿using StudioKit.ErrorHandling.WebApi;
using System.Net;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace StudioKit.Web.Http.Filters
{
	public class AuthorizeRolesAttribute : AuthorizeAttribute
	{
		public AuthorizeRolesAttribute(params string[] roles)
		{
			Roles = string.Join(",", roles);
		}

		protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
		{
			actionContext.Response =
				actionContext.Request.CreateResponse(new ApiErrorMessage(HttpStatusCode.Forbidden, "Access Denied",
					"Your request has been denied. You do not have access."));
			base.HandleUnauthorizedRequest(actionContext);
		}
	}
}